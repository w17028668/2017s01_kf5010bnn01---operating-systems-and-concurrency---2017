#include <assert.h>
#include <pthread.h>
#include "tunnel_controller.h"
#include "console.h"

static int n_in_tunnel[2];
static unsigned long n_total[2];
static int n_direction[2];

static pthread_mutex_t mutex;
static pthread_cond_t fTunnel;//conditional varibales 
static pthread_cond_t eTunnel;

//init the monitor
void safe_buffer_init(void) {
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&fTunnel, NULL);
	pthread_cond_init(&eTunnel, NULL);
}

/**
 * @brief controls entry to the tunnel
 * @param dir the direction of travel when entering
 */
void enter_tunnel(direction_t dir) {

	pthread_mutex_lock(&mutex);	
	//checks if there are cars coming in opposite directions
	while(n_in_tunnel[opposite(dir)] > 0) {
		pthread_cond_wait(&eTunnel, &mutex);
	}
	//no more than 6 in a row if the other ones are waiting
	while(n_in_tunnel[dir] <  3 && n_direction[dir] < 6) {
		n_in_tunnel[dir] += 1;
		n_direction[dir] += 1;

		assert(n_in_tunnel[dir] <= 3);
		assert(n_direction[dir] <= 6);
	}		
    
	lcd_write_at(4+dir, 0, "In tunnel (%s) : %2d", (dir == NORTH) ? "NORTH" : "SOUTH", n_in_tunnel[dir]);
    
    assert(n_in_tunnel[opposite(dir)] == 0);

   	n_direction[dir] = 0;

    pthread_cond_signal(&fTunnel);
    pthread_mutex_unlock(&mutex);
}

/**
 * @brief controls exit from the tunnel
 * @param dir the direction of travel when leaving
 */
void exit_tunnel(direction_t dir) {
	pthread_mutex_lock(&mutex);
	
	while(n_in_tunnel[dir] == 0) {
		pthread_cond_wait(&fTunnel, &mutex);
	}

	n_in_tunnel[dir] -= 1;
    n_total[dir] += 1;

    lcd_write_at(4+dir, 0, "In tunnel (%s) : %2d", (dir == NORTH) ? "NORTH" : "SOUTH", n_in_tunnel[dir]);
    lcd_write_at(6+dir, 0, "Total     (%s) : %2d", (dir == NORTH) ? "NORTH" : "SOUTH", n_total[dir]);

    pthread_cond_signal(&eTunnel);
    pthread_mutex_unlock(&mutex);
}

/**
 * @brief Gives the opposite direction to its argument
 * @param dir a direction, either NORTH or SOUTH
 * @returns the opposite of direction dir
 */
direction_t opposite(direction_t dir) {
    return ((dir == NORTH) ? SOUTH : NORTH);
}
